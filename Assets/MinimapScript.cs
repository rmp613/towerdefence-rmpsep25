﻿using UnityEngine;
using System.Collections;

public class MinimapScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 playerPosition = GameObject.FindGameObjectWithTag ("Player").transform.position;
		this.transform.position = new Vector3 (playerPosition.x, 100, playerPosition.z);
	
	}
}
