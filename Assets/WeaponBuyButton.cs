﻿using UnityEngine;
using System.Collections;

public class WeaponBuyButton : BuyButton {

	public GameObject weaponToBuy;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void OnClick(){
		WeaponManger weaponManager = GameObject.FindGameObjectWithTag ("Player").GetComponent<WeaponManger>();
		weaponManager.GetComponent<WeaponManger>().EquipWeapon (weaponToBuy.GetComponent<BaseWeapon>());
		base.OnMouseDown ();
	}

	/*
	protected override void OnMouseDown ()
	{
		GameObject weaponManager = GameObject.FindGameObjectWithTag ("WeaponManger");
		weaponManager.GetComponent<WeaponManger>().EquipWeapon (weaponToBuy.GetComponent<BaseWeapon>());
		base.OnMouseDown ();
	}
	*/
}
