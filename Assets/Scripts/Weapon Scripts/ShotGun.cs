﻿using UnityEngine;
using System.Collections;

public class ShotGun : BaseWeapon {

	GameObject ch;

	bool isFiring = false;
	bool isADS = false;

	// Use this for initialization
	void Start ()
	{
		gunAnim = GetComponent<Animation> ();
		gunAnim.Play ("Draw");
		ch = GameObject.FindGameObjectWithTag ("Crosshair");
		ch.SetActive (true);
		gunSound = GetComponent<AudioSource> ();
	}

	// Update is called once per frame
	void Update ()
	{
		UpdateAmmo ();

		if (!gunAnim.isPlaying) {

			if (currentAmmo > 0) {

				if ((Input.GetButtonDown ("Fire1") || Input.GetMouseButtonDown (0))) {
					isFiring = true;
				} else if ((Input.GetButtonUp ("Fire1") || Input.GetMouseButtonUp (0))) {
					isFiring = false;
				}
			}

			//Own Reload bit

			if (Input.GetKeyDown (KeyCode.R) && currentExtraAmmo > 0 && currentAmmo < maxClipSize) {
				Reload ();
			}

			if (Input.GetKeyDown (KeyCode.R) && currentExtraAmmo > 0 && currentAmmo < maxClipSize) {
				Reload ();
			}

			if (Input.GetButtonDown ("Fire2")) {
				if (isADS) {
					gunAnim.Play ("UnADS");
					isADS = false;
				} else {
					gunAnim.Play ("ADS");
					isADS = true;
				}
			}


			if ((Input.GetButtonDown ("Fire1") || Input.GetMouseButtonDown (0)) && currentAmmo > 0) {
				AudioSource gunSound = GetComponent<AudioSource> ();
				if (isADS) {
					gunAnim.Play ("ADSFire");

				} else {
					gunAnim.Play ("Hipfire");
				}
				((PlayerShoot)FindObjectOfType (typeof(PlayerShoot))).ShotGunShoot();
				muzzleFlash.Play ();
				gunSound.Play ();
				currentAmmo--;

			}
		}
	}

	public void Reload ()
	{
		gunAnim.Play ("ReloadUp");
		for (int i = currentAmmo; i < maxClipSize; i++) {
			gunAnim.PlayQueued ("ReloadShell");
			currentAmmo += 1;
			currentExtraAmmo -= 1;
		}
		gunAnim.PlayQueued ("ReloadDown");
	}
}
