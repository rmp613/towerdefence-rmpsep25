﻿using UnityEngine;
using System.Collections;

public class PickupWeaponScript : MonoBehaviour {

	bool canPickUp = false;

	GameObject player;

	public BaseWeapon weapon;
	
	// Update is called once per frame
	void Update () {
		if (canPickUp) {
			
			if (Input.GetKeyDown (KeyCode.E)) {
				
				if (player != null) {

					player.GetComponent<WeaponManger> ().EquipWeapon ((BaseWeapon)weapon);
					Destroy (this.gameObject);


					/*
					//Spawn the weapon as a child of the MainCamera (otherwise, the weapon doesn't follow the camera).
					GameObject mainCamera = null;

					for (int i = 0; i < 5; i++) {
						if (player.transform.GetChild (i).gameObject.tag == "MainCamera") {
							mainCamera = player.transform.GetChild (i).gameObject;
							GameObject gunholder = mainCamera.transform.GetChild (0).gameObject;

							gunholder.transform.GetChild (0).gameObject.SetActive(false); // This removes currently held weapon.

							GameObject weaponObject = (GameObject)Instantiate (weapon, gunholder.transform.position, gunholder.transform.rotation);
							weaponObject.transform.parent = gunholder.transform;
							Destroy (this.gameObject);
							break;
						}
					}*/ 

				}
			}
		}

	}

	//if the player is near enough to the gun, they will be able to pick it up
	void OnTriggerEnter(Collider other){
		if (other.tag == "Player") {
			player = other.gameObject;
			Debug.Log ("You are in range now to pickup"); 
			canPickUp = true;
		}
	}
	//Once the player if far away from the gun, they will not be able to reach it
	void OnTriggerExit(Collider other){
		if(other.tag == "Player")
			canPickUp = false;
	}
}
