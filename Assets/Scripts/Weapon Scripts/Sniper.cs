﻿using UnityEngine;
using System.Collections;

public class Sniper : BaseWeapon {

	GameObject ch;


	bool isFiring = false;
	bool isADS = false;

	// Use this for initialization
	void Start ()
	{
		gunAnim = GetComponent<Animation> ();
		gunAnim.Play ("Draw");
		ch = GameObject.FindGameObjectWithTag ("Crosshair");
		ch.SetActive (true);
		gunSound = GetComponent<AudioSource> ();
	}

	void OnEnable(){
		gunAnim.Stop ();
		gunAnim.Play ("Draw");
	}

	// Update is called once per frame
	void Update ()
	{
		UpdateAmmo ();

		if (!gunAnim.isPlaying) {
			//Debug.Log ("ANIMATION IS NOT PLAYING");
			if (currentAmmo > 0) {
				if ((Input.GetButtonDown ("Fire1") || Input.GetMouseButtonDown (0))) {
					isFiring = true;
				} else if ((Input.GetButtonUp ("Fire1") || Input.GetMouseButtonUp (0))) {
					isFiring = false;
				}

			}

			if (Input.GetKeyDown (KeyCode.R) && currentExtraAmmo > 0 && currentAmmo < maxClipSize) {
				Reload ();
			}

			if (Input.GetButtonDown ("Fire2")) {
				if (isADS) {
					gunAnim.Play ("UnADS");
					isADS = false;
				} else {
					gunAnim.Play ("ADS");
					isADS = true;
				}
			}


			if ((Input.GetButtonDown ("Fire1") || Input.GetMouseButtonDown (0)) && currentAmmo > 0) {
				AudioSource gunSound = GetComponent<AudioSource> ();
				if (isADS) {
					//((PlayerShoot)FindObjectOfType (typeof(PlayerShoot))).Shoot ();
					gunAnim.Play ("ADS Fire");
					gunAnim.PlayQueued ("UnADS");
					gunAnim.PlayQueued ("Unload");
					gunAnim.PlayQueued ("ADS");
				} else {
					//((PlayerShoot)FindObjectOfType (typeof(PlayerShoot))).Shoot ();
					gunAnim.Play ("Hipfire");
					gunAnim.PlayQueued ("Unload");
				}
				((PlayerShoot)FindObjectOfType (typeof(PlayerShoot))).Shoot ();
				muzzleFlash.Play ();
				gunSound.Play ();
				currentAmmo--;

			}
		}
	}

	public void Reload ()
	{
		currentAmmo = currentExtraAmmo + currentAmmo;

		if (currentAmmo >= maxClipSize) {
			gunAnim.Play ("Reload");
			gunAnim.PlayQueued ("Unload");
			currentExtraAmmo -= maxClipSize - currentAmmo;
			currentAmmo = maxClipSize;
		}
	}
}
