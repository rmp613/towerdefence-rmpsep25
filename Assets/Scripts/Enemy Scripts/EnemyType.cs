﻿using UnityEngine;
using System.Collections;

public class EnemyType{
	
	public string name;
	public Enemy.Target target;
	public GameObject prefab;
	public int amount = 1;

	public EnemyType(string name, Enemy.Target target, GameObject prefab){
		this.name = name;
		this.target = target;
		this.prefab = prefab;
	}

	public EnemyType(string name, Enemy.Target target, GameObject prefab, int amount){
		this.name = name;
		this.target = target;
		this.prefab = prefab;
		this.amount = amount;
	}
}

