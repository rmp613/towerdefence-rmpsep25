﻿using UnityEngine;
using System.Collections;

public class TowerHitScanSystem : MonoBehaviour
{

	public float attackRate;
	public int damage;
	public float dps;
	float attacksPerSecond;

	public GameObject target;
	public float fieldOfView;

	// Use this for initialization
	void Start()
	{
	}

	// Update is called once per frame
	void Update()
	{
		attacksPerSecond = 1 / attackRate;
		dps = (float)damage * attacksPerSecond;

		target = gameObject.GetComponent<TowerTracking>().target;
		if (gameObject.GetComponent<TowerTracking>().trackCount > 0)
		{
			{
				//float angle = Quaternion.Angle(transform.rotation, Quaternion.LookRotation(target.transform.position - transform.position));
				float angle = Quaternion.Angle(transform.FindChild("Pivot").rotation, Quaternion.LookRotation(target.transform.position - transform.position));

				if (angle < fieldOfView)
				{
					Debug.Log (target.gameObject.GetComponent<Enemy> ().currentHealth);
					target.gameObject.GetComponent<Enemy>().CmdTakeDamage(damage);
					Debug.Log (target.gameObject.GetComponent<Enemy> ().currentHealth);

				}

			}
		}
	}
}