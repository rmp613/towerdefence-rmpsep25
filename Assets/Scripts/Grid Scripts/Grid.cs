﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

//README:
//Don't be confused by my use of .x and .y rather than .x and .z as I used a Vector 2 to input the gridsize
//In reality I am using the x and z axis and not touching the y axis at all really 
public class Grid : NetworkBehaviour {
	//public static LayerMask unwalkableMask;
	public static Vector2 gridWorldSize;
	public static float nodeRadius;
	public static GridNode[,] grid;
	static Vector3 worldBottomLeft;

	public static float nodeDiameter;
	public static int gridSizeX, gridSizeY;

	[SerializeField]
	private LayerMask mapLayer;

	public static Grid singleton; 


	void Awake(){
		if (singleton != null) {
			Debug.LogError ("More than one Grid is running"); 
		} else {
			singleton = this; 
			//change this to change size of grid:
			gridWorldSize = new Vector2(102, 102);
			//unwalkableMask = LayerMask.NameToLayer ("Unwalkable");
			nodeRadius = .5f;
			nodeDiameter = nodeRadius * 2;
			gridSizeX = Mathf.RoundToInt (gridWorldSize.x / nodeDiameter);
			gridSizeY = Mathf.RoundToInt (gridWorldSize.y / nodeDiameter);
			CreateGrid ();
			//singletons make it very easy to gain access to the Gamemanager without any gameobject.find or similar things. 
		}
	}

	//Returns the node on the Grid that a point in the world corresponds to.
	//currently doesnt take into account the y axis but if we add in slopes etc to our maps later ill fix that.
	public static GridNode GetNode(Vector3 worldPos){
		float perX = (worldPos.x + gridWorldSize.x/2) /gridWorldSize.x;
		float perY = (worldPos.z + gridWorldSize.y/2) /gridWorldSize.y;
		perX = Mathf.Clamp01(perX);
		perY = Mathf.Clamp01(perY);

		int x = Mathf.RoundToInt((gridSizeX-1) * perX);
		int y = Mathf.RoundToInt((gridSizeY-1) * perY);
		if (x < gridSizeX && y < gridSizeY) {
			return grid[x, y];
		} else {
			return default(GridNode);
		}
	}
	public static Vector2 GetVector2(Vector3 worldPos){
		float perX = (worldPos.x + gridWorldSize.x/2) /gridWorldSize.x;
		float perY = (worldPos.z + gridWorldSize.y/2) /gridWorldSize.y;
		perX = Mathf.Clamp01(perX);
		perY = Mathf.Clamp01(perY);

		int x = Mathf.RoundToInt((gridSizeX-1) * perX);
		int y = Mathf.RoundToInt((gridSizeY-1) * perY);
		if (x < gridSizeX && y < gridSizeY) {
			return new Vector2 (x, y);
		}
		return new Vector2 ();
	}
	public static bool GetWalkable(int x, int y){
		return grid [x, y].walkable;
	}
	public static void SetUnwalkable(int x, int y){
		grid [x, y].walkable = false;
	}
    public static void SetWalkable(int x, int y) {
        grid[x, y].walkable = true;
    }
    private static void CreateGrid(){
		grid = new GridNode[gridSizeX, gridSizeY];
		worldBottomLeft = -Vector3.right * (gridWorldSize.x / 2) - Vector3.forward * (gridWorldSize.y / 2);
		for(int x = 0; x < gridSizeX; x++){
			for(int y = 0; y < gridSizeY; y++){
				Vector3 worldPoint = GetWorldPos (x, y);
				bool walkable = !(Physics.CheckBox (new Vector3(worldPoint.x, 1f, worldPoint.z), new Vector3(nodeRadius-0.1f, .75f, nodeRadius-0.1f), Quaternion.identity, singleton.mapLayer));
				grid[x, y] = new GridNode(walkable, worldPoint);
				if (gridSizeX / 2 == x && gridSizeY / 2 == y) {
				}
			}
		}
	}
	public static Vector3 GetWorldPos(int x, int y){
		return worldBottomLeft + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.forward * (y * nodeDiameter + nodeRadius);
	}
}
